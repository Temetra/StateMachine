﻿using System;
using System.Collections.Generic;

namespace StateMachine
{
	public delegate void StateTransition();

	internal static class StateMachineMessages
	{
		public static string CollectionAlreadyContainsKey { get { return "A transition with the same key already exists in the collection"; } }
		public static string AlreadyInState { get { return "Already in state transitioning to"; } }
		public static string TransitionNotFound { get { return "Transition was not found in collection"; } }
	}

	public class StateMachine<TStateId> where TStateId : IComparable
	{
		private TStateId currentState = default(TStateId);
		private Dictionary<int, StateTransition> transitions = new Dictionary<int, StateTransition>();

		/// <summary>
		/// Current state ID of machine
		/// </summary>
		public TStateId CurrentState
		{
			get { return currentState; }
		}

		/// <summary>
		/// Add a transition to collection
		/// </summary>
		/// <param name="from">State transitioning from</param>
		/// <param name="to">State transitioning to</param>
		/// <param name="transition">Method to call when transitioning</param>
		/// <exception cref="ArgumentNullException">from, to or transition is null</exception>
		/// <exception cref="ArgumentException">A transition with the same key already exists in the collection</exception>
		public void AddTransition(TStateId from, TStateId to, StateTransition transition)
		{
			// Validate input
			if (from == null) throw new ArgumentNullException("from");
			if (to == null) throw new ArgumentNullException("to");
			if (transition == null) throw new ArgumentNullException("transition");

			// Create a key
			int key = CreateTransitionHash(from, to);
			if (transitions.ContainsKey(key)) throw new ArgumentException(StateMachineMessages.CollectionAlreadyContainsKey);

			// Store transition
			transitions.Add(key, transition);
		}

		/// <param name="to">State transitioning to</param>
		/// <exception cref="ArgumentNullException">State transitioning to is null</exception>
		/// <exception cref="ArgumentException">Already in state transitioning to</exception>
		/// <exception cref="ArgumentException">Transition was not found in collection</exception>
		public void PerformTransition(TStateId to)
		{
			// Validate input
			if (to == null) throw new ArgumentNullException("to");
			if (to.CompareTo(currentState) == 0) throw new ArgumentException(StateMachineMessages.AlreadyInState);

			// Create a key
			int key = CreateTransitionHash(currentState, to);
			if (transitions.ContainsKey(key) == false) throw new ArgumentException(StateMachineMessages.TransitionNotFound);

			// Execute transition
			transitions[key].Invoke();
			currentState = to;
		}

		private int CreateTransitionHash(TStateId from, TStateId to)
		{
			unchecked
			{
				int hash = 23;
				hash = hash * 31 + from.GetHashCode();
				hash = hash * 31 + to.GetHashCode();
				return hash;
			}
		}
	}
}
