﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using StateMachine;

namespace StateMachineTesting
{
	[TestClass]
	public class AddSingleTransitionTest
	{
		[TestMethod]
		public void AddSingleTransition()
		{
			// Set up
			var fsm = new StateMachine<TestStates>();

			// Execute
			fsm.AddTransition(TestStates.First, TestStates.Second, StateTransition);

			// Assert
			Assert.AreEqual(TestStates.None, fsm.CurrentState);
		}

		[TestMethod]
		public void NullFromState()
		{
			// Set up
			var fsm = new StateMachine<string>();

			// Execute
			var ex = ExceptionAssert.Throws<ArgumentNullException>(() => fsm.AddTransition(null, "First", StateTransition));

			// Assert
			Assert.AreEqual("from", ex.ParamName);
		}

		[TestMethod]
		public void NullToState()
		{
			// Set up
			var fsm = new StateMachine<string>();

			// Execute
			var ex = ExceptionAssert.Throws<ArgumentNullException>(() => fsm.AddTransition("None", null, StateTransition));

			// Assert
			Assert.AreEqual("to", ex.ParamName);
		}

		[TestMethod]
		public void NullTransition()
		{
			// Set up
			var fsm = new StateMachine<TestStates>();

			// Execute
			var ex = ExceptionAssert.Throws<ArgumentNullException>(() => fsm.AddTransition(TestStates.None, TestStates.First, null));

			// Assert
			Assert.AreEqual("transition", ex.ParamName);
		}

		[TestMethod]
		public void TransitionAlreadyExists()
		{
			// Set up
			var fsm = new StateMachine<TestStates>();
			fsm.AddTransition(TestStates.None, TestStates.First, StateTransition);

			// Execute
			var ex = ExceptionAssert.Throws<ArgumentException>(() => fsm.AddTransition(TestStates.None, TestStates.First, StateTransition));

			// Assert
			Assert.AreEqual("A transition with the same key already exists in the collection", ex.Message);
		}

		private void StateTransition()
		{
		}
	}
}
