﻿
namespace StateMachineTesting
{
	public enum TestStates
	{
		None = 0,
		First,
		Second,
		Third,
		Fake1,
		Fake2,
		Fake3,
		Fake4,
	}
}
