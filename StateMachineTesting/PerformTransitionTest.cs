﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using StateMachine;

namespace StateMachineTesting
{
	[TestClass]
	public class PerformTransitionTest
	{
		private bool noneToFirst = false;

		[TestMethod]
		public void NoneToFirstTransition()
		{
			// Set up
			var fsm = new StateMachine<TestStates>();
			fsm.AddTransition(TestStates.None, TestStates.First, NoneToFirst);
			Assert.AreEqual(TestStates.None, fsm.CurrentState);
			Assert.IsFalse(noneToFirst);

			// Execute
			fsm.PerformTransition(TestStates.First);

			// Assert
			Assert.AreEqual(TestStates.First, fsm.CurrentState);
			Assert.IsTrue(noneToFirst);
		}

		[TestMethod]
		public void InvalidTransition()
		{
			// Set up
			var fsm = new StateMachine<TestStates>();
			fsm.AddTransition(TestStates.None, TestStates.First, NoneToFirst);
			fsm.PerformTransition(TestStates.First);
			Assert.AreEqual(TestStates.First, fsm.CurrentState);

			// Execute
			var ex = ExceptionAssert.Throws<ArgumentException>(() => fsm.PerformTransition(TestStates.First));

			// Assert
			Assert.AreEqual("Already in state transitioning to", ex.Message);
		}

		[TestMethod]
		public void TransitionNotFound()
		{
			// Set up
			var fsm = new StateMachine<TestStates>();

			// Execute
			var ex = ExceptionAssert.Throws<ArgumentException>(() => fsm.PerformTransition(TestStates.First));

			// Assert
			Assert.AreEqual("Transition was not found in collection", ex.Message);
		}

		private void NoneToFirst()
		{
			noneToFirst = true;
		}
	}
}
